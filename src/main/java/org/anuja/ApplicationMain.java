package org.anuja;

import org.anuja.data.Product;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ApplicationMain {

	
	public static void main(String[] args) {
		ConfigurableApplicationContext context= SpringApplication.run(ApplicationMain.class, args);
		
		Product product=context.getBean(Product.class);
		
		
		product.productName="Pen";
		product.price=(double) 5;
		product.quantity=100;
		
		product.category.name="Stationary";
		
		product.variations.VariationList.add("Red Color");
		product.variations.VariationList.add("Pink color");
		
		product.PrintData();
		
	}
}
